{ buildPackages
, callPackage
, perl
, bison ? null
, flex ? null
, gmp ? null
, libmpc ? null
, mpfr ? null
, lib
, writeText
, stdenv
, runCommand
, linuxManualConfig
, pkg-config
, ncurses

, # The kernel source tarball.
  src

, # The kernel version.
  version

, # Allows overriding the default defconfig
  defconfig ? null

, # The version number used for the module directory
  modDirVersion ? version

, # Custom seed used for CONFIG_GCC_PLUGIN_RANDSTRUCT if enabled. This is
  # automatically extended with extra per-version and per-config values.
  randstructSeed ? ""

, kernelPatches ? []

, configFragments ? []

, extraMeta ? {}

# easy overrides to stdenv.hostPlatform.linux-kernel members
, kernelArch ? stdenv.hostPlatform.linuxArch

, ...
}:

assert stdenv.isLinux;

let
  inherit (lib) concatMap optional toList;
  inherit (builtins) filter;

  #
  # TODO: Copy-pasted from nixpkgs/pkgs/os-specific/linux/kernel/manual-config.nix
  # Getting infinite recursion using `allowImportsFromDerivation`
  #
  readConfig = configfile: import (runCommand "config.nix" {} ''
    echo "{" > "$out"
    while IFS='=' read key val; do
      [ "x''${key#CONFIG_}" != "x$key" ] || continue
      no_firstquote="''${val#\"}";
      echo '  "'"$key"'" = "'"''${no_firstquote%\"}"'";' >> "$out"
    done < "${configfile}"
    echo "}" >> $out
  '').outPath;

  configfile = stdenv.mkDerivation {
    inherit src version kernelArch;

    patches = map (p: p.patch) kernelPatches;

    pname = "linux-config";

    depsBuildBuild = [ buildPackages.stdenv.cc ];

    nativeBuildInputs = [ perl gmp libmpc mpfr ]
      ++ lib.optionals (lib.versionAtLeast version "4.16") [ bison flex ];

    platformName = stdenv.hostPlatform.linux-kernel.name;
    # e.g. "defconfig"
    kernelBaseConfig = if defconfig != null then defconfig else stdenv.hostPlatform.linux-kernel.baseConfig;
    # e.g. "bzImage"
    kernelTarget = stdenv.hostPlatform.linux-kernel.target;

    configFragment = (lib.concatStringsSep "\n" (toList configFragments));
    passAsFile = [ "configFragment" ];

    buildPhase = ''
      export buildRoot="''${buildRoot:-build}"
      export ARCH=$kernelArch
      export HOSTCC=${buildPackages.stdenv.cc.targetPrefix}gcc \
      export HOSTCXX=${buildPackages.stdenv.cc.targetPrefix}g++

      set -x
      mkdir -p "$buildRoot"
      cp "arch/$kernelArch/configs/$kernelBaseConfig" "$buildRoot/.config"
      make -C . O="$buildRoot" $kernelBaseConfig

      source ./scripts/kconfig/merge_config.sh -r -O "$buildRoot" "$buildRoot/.config" $configFragmentPath

      #
      # The last section of the merge_config.sh script doesn't fail properly
      # so the check is done again here.
      #
      for CFG in $(sed -n -e "$SED_CONFIG_EXP1" -e "$SED_CONFIG_EXP2" $TMP_FILE); do
          REQUESTED_VAL=$(grep -w -e "$CFG" $TMP_FILE)
          ACTUAL_VAL=$(grep -w -e "$CFG" "$KCONFIG_CONFIG" || true)
          if [ "x$REQUESTED_VAL" != "x$ACTUAL_VAL" ] ; then
            exit 1
          fi
      done
    '';

    installPhase = "mv $buildRoot/.config $out";

    enableParallelBuilding = true;

    passthru = rec {};
  }; # end of configfile derivation

  kernel = (linuxManualConfig {
    inherit version modDirVersion src kernelPatches randstructSeed lib stdenv extraMeta configfile;
  }).overrideAttrs (kernel: {
    nativeBuildInputs = (kernel.nativeBuildInputs or []) ++ [ pkg-config ncurses ];
    postInstall = (kernel.postInstall or "") + "ln -s ${configfile} $out/defconfig";
  });

  passthru = {
    kernelOlder = lib.versionOlder version;
    kernelAtLeast = lib.versionAtLeast version;
    passthru = kernel.passthru // (removeAttrs passthru [ "passthru" ]);
  };

in lib.extendDerivation true passthru kernel
