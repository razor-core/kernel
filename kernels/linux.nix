{ version
, src
, name ? "linux-${builtins.replaceStrings ["."] ["_"] version}"
, modDirVersion ? version
}:
final: prev: {
  ${name} = prev.callPackage ./generic.nix {
    inherit src version modDirVersion;
    nixpkgs = prev.path;
    kernelPatches = [];
    configFragments = [ "CONFIG_MODULES=y" ];
  };
}
