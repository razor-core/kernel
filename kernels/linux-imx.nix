{ vendor
, version
, src
, name ? "linux-${vendor}-${builtins.replaceStrings ["."] ["_"] version}"
}:
final: prev: {
  ${name} = prev.callPackage ./generic.nix {
    inherit src version;
    nixpkgs = prev.path;
    kernelPatches = [];
    configFragments = [ "CONFIG_MODULES=y" ];
  };
}
