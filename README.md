# razor-core kernels

This repository contains the different linux kernels provided by chip suppliers, and mainline kernels.

To manually build a kernel do

```
nix flake .#linux-imx-5_4_70.aarch64-linux
```

