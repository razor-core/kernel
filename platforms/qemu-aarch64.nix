kernels: final: prev: prev.lib.recursiveUpdate prev {
  pkgsCross.qemu-aarch64 = import prev.path {
    localSystem = prev.buildPlatform.system;
    crossSystem = {
      config = "aarch64-unknown-linux-gnu";
      gcc.arch = "armv8-a";
      linux-kernel.name = "linux";
      linux-kernel.baseConfig = "defconfig";
      linux-kernel.DTB = true;
      linux-kernel.autoModules = true;
      linux-kernel.preferBuiltin = true;
      linux-kernel.configFragments = [];
      linux-kernel.target = "Image";
    };
    inherit (prev) overlays;
    crossOverlays = map (import ../kernels/linux.nix) kernels;
  };
}
