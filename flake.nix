{
  description = "razor-core kernel templates and examples";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";

    linux-imx-5_4_70.url = "git+https://source.codeaurora.org/external/imx/linux-imx?ref=imx_5.4.70_2.3.0";
    linux-imx-5_4_70.flake = false;

    # linux-5_4.url = "github:torvalds/linux/v5.11";
    # linux-5_4.flake = false;

  };

  outputs = args@{ self, nixpkgs, flake-utils, ... }:
    let
      inherit (nixpkgs.lib.lists) fold;
      inherit (nixpkgs.lib.attrsets) recursiveUpdate attrValues;
      inherit (flake-utils.lib) flattenTree;

      # All the platform/kernel configurations enumerated
      platforms = map ({name, overlay, kernels}: { ${name} = import overlay kernels; }) [
        {
          name = "imx8mn";
          overlay = ./platforms/imx8mn.nix;
          kernels = with args; [
            { vendor = "imx"; version = "5.4.70"; src = linux-imx-5_4_70; }
          ];
        }
        # {
        #   name = "qemu-aarch64";
        #   overlay = ./platforms/qemu-aarch64.nix;
        #   kernels = with args; [
        #     { version = "5.4"; src = linux-5_4; }
        #   ];
        # }
      ];

      # We don't want the standard pkgsCross from nixpkgs since it adds confusion
      resetPkgsCross = _: _: { pkgsCross = {}; };

    in
    rec {

      overlays = fold recursiveUpdate {} platforms;

      legacyPackages.x86_64-linux = import nixpkgs {
        localSystem = "x86_64-linux";
        overlays = [ resetPkgsCross ] ++ attrValues overlays;
      };

      checks.x86_64-linux = {
        linux-imx-5_4_70-imx8mn = legacyPackages.x86_64-linux.pkgsCross.imx8mn.linux-imx-5_4_70;
        # linux-5_4-qemu-aarch64 = legacyPackages.x86_64-linux.pkgsCross.qemu-aarch64.linux-5_4;
      };
    };
}
